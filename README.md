# Consigne d'installation

Script to init awesome project on docker with symfony 4 api rest, reactJs, Mysql, nginx

## First Step
----
 - Get the init repo for project
```
git clone git@gitlab.com:Horsty/install-project.git
cd install-project
```
- Make install runnable
- Launch install depending on our OS
  - ### For linux / Os X
    ---
    ```
    chmod +x install.sh
    ./install.sh
    ```
  - ### For Windows
    ---
    ```
    chmod +x install_win.sh
    ./install_win.sh
    ```
- Go to :
   - `http://back.horsty.test` => API (Symfony4)
   - `http://app.horsty.test:3000` => APP (ReactJs)
   - `localhost:8080` => ADMINER (Sgbd)
- Enjoy !!!