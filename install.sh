#Create Project
sudo rm -rf ../project/

#Clone Project
git clone git@gitlab.com:Horsty/awesome-project.git ../project
cd ../project
git clone git@gitlab.com:Horsty/dbz-app.git 
git clone git@gitlab.com:Horsty/back.git api

#Install docker-compose
#TODO Si docker installé sur mac pas besoin de docker-compose
curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o ~/docker-compose
chmod +x ~/docker-compose 
sudo mv ~/docker-compose /usr/local/bin/docker-compose 

#Add new host for nginx conf
#TODO modifier sudo tee --append ne fonctionne pas
grep 'app.horsty.dev' /etc/hosts || echo "127.0.0.1  app.horsty.dev" | sudo tee --append /etc/hosts
grep 'back.horsty.dev' /etc/hosts || echo "127.0.0.1  back.horsty.dev" | sudo tee --append /etc/hosts

#Install composer
cd ./api
cp .env .env.local

#Kill containers
cd ..

mkdir -p ./logs/nginx/api
mkdir -p ./logs/nginx/dbz-app
#Stop container
docker-compose down -v

#Build containers
docker-compose build
docker-compose up -d

#Install composer on symfony project
docker exec -it project_php_1 composer install
