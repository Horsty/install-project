#Create Project
rm -rf ../project/

#Clone Project
git clone git@gitlab.com:Horsty/awesome-project.git ../project
cd ../project
git clone git@gitlab.com:Horsty/dbz-app.git 
git clone git@gitlab.com:Horsty/back.git api

#Install docker-compose
curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o ~/docker-compose
chmod +x ~/docker-compose 


#Add new host for nginx conf
grep 'app.horsty.test' C:/Windows/System32/drivers/etc/hosts || echo "127.0.0.1  app.horsty.test" | tee --append C:/Windows/System32/drivers/etc/hosts
grep 'back.horsty.test' C:/Windows/System32/drivers/etc/hosts || echo "127.0.0.1  back.horsty.test" | tee --append C:/Windows/System32/drivers/etc/hosts

#Install api
cd ./api
cp .env .env.local

## Kill Volume
cd ..

mkdir -p ./logs/nginx/api
mkdir -p ./logs/nginx/dbz-app
#Stop container
docker-compose down -v

#Build containers
docker-compose build
docker-compose up -d

#Install composer on symfony project
winpty docker exec -it project_php_1 composer install
